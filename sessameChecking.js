const fs = require('fs');
const puppeteer = require('puppeteer');
const Spinner = require('cli-spinner').Spinner;
const argv = require('yargs/yargs')(process.argv.slice(2))
    .usage('Usage: $0 -u [string] -p [string]')
    .describe('u', 'Sessame email (user)')
    .describe('p', 'Sessame password')
    .demandOption(['u','p'])
    .argv;

const cnf = {
    URL: 'https://app.sesametime.com',
    $INPUT_EMAIL: 'input[type="email"]',
    $BTN_EMAIL_NEXT: '.remember + button',
    $INPUT_PASSWORD: 'input[type="password"]',
    BTN_PASSWORD_TEXT: 'login',
    BTN_CHECKIN_TEXT: 'entrar',
};


async function findElemntByTextAndClick(page, selector, textLowerCase) {
    // const texts = await page.$$eval(selector, el => el.map(l => l.innerText));
    // console.log(texts)
    return await page.$$eval(selector, (els, { textLowerCase }) => {
    const btn = els.find(el => {
        return el.innerText.trim().toLowerCase() === textLowerCase.trim().toLowerCase();
    });
    btn.click();
   }, { textLowerCase });
}

(async () => {
    const spinner = new Spinner('Opening browser in headless mode.. %s');
    spinner.setSpinnerString('|/-\\');
    spinner.start();
    try {
        const browser = await puppeteer.launch({
            headless: true,
            args: [
                '--user-agent=Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.81 Safari/537.36'
            ],
        });
        const page = await browser.newPage();
        await page.setViewport({
            width: 1280,
            height: 800,
        });
        spinner.setSpinnerTitle('Loging...(min 8s) %s');
        await page.goto(cnf.URL, { waitUntil: 'networkidle2' });
        await page.type(cnf.$INPUT_EMAIL, argv.u);
        await page.click(cnf.$BTN_EMAIL_NEXT);
        await page.waitForSelector(cnf.$INPUT_PASSWORD);
        await page.type(cnf.$INPUT_PASSWORD, argv.p);
        await findElemntByTextAndClick(page, 'button', cnf.BTN_PASSWORD_TEXT); 
        await page.waitForTimeout(8000);
        spinner.setSpinnerTitle('Checking...(min 8s) %s');
        await findElemntByTextAndClick(page, 'button', cnf.BTN_CHECKIN_TEXT); 
        await page.waitForTimeout(8000);
        const now = new Date();
        const filename = `${now.getDate()}-${now.getMonth()}-${now.getFullYear()}_${now.getHours()}${now.getMinutes()}`;
        spinner.stop();
        console.log(`Taking screenshot names ./logs/${filename}.png`);
        try {
            fs.accessSync('./logs'); 
        } catch {
            fs.mkdirSync('./logs');
        }
        await page.screenshot({ path: `./logs/${filename}.png` })
        await browser.close();
    } catch {
        spinner.stop();
        console.error('Esto se ha ido al pedo. Comprueba si ya hiciste checkin.');
        process.exit(-1);
    }
})();